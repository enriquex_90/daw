--VENTAS POR VENDEDOR FILTRADO POR OFICINA --
select 
sum(ve_monto)  as MONTO,
(select ve_descripcion from tr_vendedor v where v.id =a.ve_vendedor_id and v.ve_oficina_id = a.ve_oficina_id) as NOMBRE_VENDEDOR,
(case  substr(ve_fecha,1,2) when '01' then 'ENERO'
                            when '02' then 'FEBRERO'
                            when '03' then 'MARZO'
                            when '04' then 'ABRIL'
                            when '05' then 'MAYO'
                            when '06' then 'JUNIO'
                            when '07' then 'JULIO'
                            when '08' then 'AGOSTO'
                            when '09' then 'SEPTIEMBRE'
                            when '10' then 'OCTUBRE'
                            when '11' then 'NOVIEMBRE'
                            when '12' then 'DICIEMBRE' else '' end )    as FECHA,
(select of_descripcion from tr_oficina where id = a.ve_oficina_id)      as OFICINA
from tr_ventas a
where ve_oficina_id in (1,2,3)  --AQUI COLOCAR LA OFICINA 1,2,3
group by NOMBRE_VENDEDOR,FECHA,OFICINA
order by FECHA



--PAQUETES  VENDIDOS -PAIS XOFI
select
count(ve_paquete_id)  as TOTALVENDIDOS,
(select ti_descripcion from tr_tipo t  ,tr_paquete b, tr_asociado a    where pa_tipo_id    =t.id and b.id= a.ve_paquete_id and pa_asociado_id = a.id ) as TIPOPAQUETE,
(select pa_descripcion from tr_pais p ,tr_cliente c where ve_cliente_id = c.id and cl_pais_id =p.id ) as PAIS_ORIGEN
from  tr_ventas a
where ve_oficina_id in (1,2,3) --AQUI COLOCAR LA OFICINA 1,2,3
group by TIPOPAQUETE,PAIS_ORIGEN
order by TOTALVENDIDOS desc



select strftime('%m','01/05/17')



--ORIGEN DE LOS TURISTAS QUE MAS COMPRAN EN TURITYFY
select
count(v.id)    as 'CANTIDAD PAQUETES VENDIDOS',
select (ve_paquete_id ,
(select pa_descripcion from tr_pais p ,tr_cliente c where ve_cliente_id = c.id and cl_pais_id =p.id ) as Pais
from tr_ventas v
where ve_oficina_id in (1,2,3)
group by Pais,ve_paquete_id

--TOP DE PREFERENCIA DE TURISTAS POR ASOCIADOS

select
count(ve_paquete_id)  as 'TOTALVENDIDOS',
(select as_descripcion from tr_asociado a ,tr_paquete p                where p.id=ve_paquete_id and pa_tipo_id = a.id )                  as 'ASOCIADO'
from  tr_ventas a
where ve_oficina_id in (1,2,3) --AQUI COLOCAR LA OFICINA 1,2,3
group by ASOCIADO


--VENDEDORES REGISTRADOS
select
ve_descripcion as 'VENDEDOR',
(select of_descripcion from tr_oficina o where o.id = v.ve_oficina_id) as 'OFICINA'
 from tr_vendedor v
 where ve_oficina_id in (1,2,3)




select * from  tr_ventas
where ve_monto >1000


update tr_oficina set of_descripcion = 'OF SAMBORONDÓN' where of_descripcion = 'OF MALL DEL SOL'


select * from tr_paquete

select distinct
sum(ve_monto)            as 'MONTO',
COUNT(ve_paquete_id)     as 'TOTAL PAQUETES',
--ve_paquete_id,
as_descripcion           as  'ASOCIADO',
ti_descripcion           as  'TIPO PAQUETE'
from  tr_paquete p, tr_tipo t , tr_asociado a ,tr_ventas v
where ve_oficina_id in (1,2,3) --AQUI COLOCAR LA OFICINA 1,2,3
and p.pa_tipo_id     = t.id
and p.pa_asociado_id = a.id
and ve_paquete_id    = p.id
group by  ve_paquete_id,ti_descripcion,as_descripcion