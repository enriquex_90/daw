--SCRIPTS DE CREACION

create table if not exists tr_ventas(
id             integer ,
ve_vendedor_id integer ,
ve_paquete_id  integer ,
ve_cliente_id  integer ,
ve_monto       integer ,
ve_fecha       date    ,
ve_oficina_id  integer);

create table if not exists tr_cliente(
id         integer,
cl_passport     text   ,
cl_nombre       text   ,
cl_pais_id      integer);


create table if not exists tr_paquete(
id                 integer,
pa_tipo_id       integer,
pa_descripcion     text,
pa_precio_pvp      money,
pa_asociado_id     int);

create table if not exists tr_vendedor(
id               integer,
ve_oficina_id    integer,
ve_descripcion   text,
ve_estado        text);

create table if not exists tr_oficina(
id                integer,
of_descripcion    integer,
of_estado         text);

create table if not exists tr_pais(
id               integer,
pa_descripcion   text);

create table if not exists tr_asociado(
id            integer,
as_descripcion  text,
as_estado       text);

create table if not exists tr_tipo(
id            integer,
ti_descripcion  text)

