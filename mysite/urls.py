"""daw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import include, url
from . import views
from django.conf.urls import url
from django.urls import path
from django.contrib.auth import views as auth_views

app_name = 'mysite'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^report/$', views.report, name='report'),
    path('json/data1/', views.data1, name='data1'),
    path('json/data2/', views.data2, name='data2'),
    path('json/data3/', views.data3, name='data3'),
    path('json/data4/', views.data4, name='data4'),
]
