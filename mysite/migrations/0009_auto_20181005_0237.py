# Generated by Django 2.0.7 on 2018-10-05 02:37

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0008_auto_20181005_0236'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ventas',
            old_name='ve_id_cliente',
            new_name='ve_cliente',
        ),
    ]
