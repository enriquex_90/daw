# Generated by Django 2.0.7 on 2018-09-28 02:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0004_auto_20180909_0058'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='city',
            name='country',
        ),
        migrations.RemoveField(
            model_name='consult',
            name='city',
        ),
        migrations.RemoveField(
            model_name='consult',
            name='country',
        ),
        migrations.DeleteModel(
            name='City',
        ),
        migrations.DeleteModel(
            name='Consult',
        ),
        migrations.DeleteModel(
            name='Country',
        ),
    ]
