# Generated by Django 2.0.7 on 2018-10-05 02:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mysite', '0010_auto_20181005_0247'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ti_descripcion', models.CharField(max_length=30)),
            ],
            options={
                'db_table': 'tr_tipo',
            },
        ),
        migrations.RemoveField(
            model_name='paquete',
            name='pa_asociado',
        ),
        migrations.RemoveField(
            model_name='ventas',
            name='ve_cliente',
        ),
        migrations.RemoveField(
            model_name='ventas',
            name='ve_oficina',
        ),
        migrations.RemoveField(
            model_name='ventas',
            name='ve_paquete',
        ),
        migrations.RemoveField(
            model_name='ventas',
            name='ve_vendedor',
        ),
        migrations.DeleteModel(
            name='Paquete',
        ),
        migrations.DeleteModel(
            name='Ventas',
        ),
    ]
