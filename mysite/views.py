from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse
from django.db import connection


def index(request):
    return render(request, 'index.html',{'menu':True})


@login_required
def report(request):
    return render(request, 'report.html')


@login_required
def data1(request):
    oficina = request.GET['point']

    with connection.cursor() as cursor:
        cursor.execute("""  select 
                            sum(ve_monto)         as 'MONTO',
                            count(ve_paquete_id)  as 'TOTALVENDIDOS',
                            (select ti_descripcion from tr_tipo t  ,tr_paquete b, tr_asociado a    where pa_tipo_id    =t.id and b.id= a.ve_paquete_id and pa_asociado_id = a.id ) as 'TIPOPAQUETE',
                            (select as_descripcion from tr_asociado a ,tr_paquete p                where p.id=ve_paquete_id and pa_tipo_id = a.id )                  as 'ASOCIADO'
                            from  tr_ventas a
                            where ve_oficina_id in (""" + oficina + """)
                            group by TIPOPAQUETE,ASOCIADO
                            order by TIPOPAQUETE asc""")
        rows = cursor.fetchall()
    data = []
    value = max([row[1] for row in rows])
    for row in rows:
        item = {}
        item['name'] = row[2]
        item['y'] = row[1]
        if row[1] == value:
            item['sliced'] = True
            item['selected'] = True
        data.append(item)

    chart = {
        'chart': {
            'plotBackgroundColor': None,
            'plotBorderWidth': None,
            'plotShadow': False,
            'type': 'pie'
        },
        'title': {
            'text': 'Paquetes mejores vendidos'
        },
        'tooltip': {
            'pointFormat': '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        'plotOptions': {
            'pie': {
                'allowPointSelect': True,
                'cursor': 'pointer',
                'dataLabels': {
                    'enabled': False
                },
                'showInLegend': True
            }
        },
        'series': [{
            'name': 'Brands',
            'colorByPoint': True,
            'data': data
        }]
    }

    return JsonResponse(chart)


@login_required
def data2(request):
    oficina = request.GET['point']
    with connection.cursor() as cursor:
        cursor.execute("""  select as_descripcion from tr_asociado""")
        asociado_rows = cursor.fetchall()

    with connection.cursor() as cursor:
        cursor.execute("""  select distinct
                            sum(ve_monto)            as 'MONTO',
                            COUNT(ve_paquete_id)     as 'TOTAL PAQUETES',
                            --ve_paquete_id,
                            as_descripcion           as  'ASOCIADO',
                            ti_descripcion           as  'TIPO PAQUETE' 
                            from  tr_paquete p, tr_tipo t , tr_asociado a ,tr_ventas v
                            where ve_oficina_id in (""" + oficina + """)
                            and p.pa_tipo_id     = t.id 
                            and p.pa_asociado_id = a.id
                            and ve_paquete_id    = p.id
                            group by  ve_paquete_id,ti_descripcion,as_descripcion""")
        rows = cursor.fetchall()
    data = []
    categories = [asociado[0] for asociado in asociado_rows]
    data.append({'name': 'BASICO', 'data': []})
    data.append({'name': 'MEDIUM', 'data': []})
    data.append({'name': 'PREMIUM', 'data': []})
    for categorie in categories:
        for data_item in data:
            data_add = 0
            for row in rows:
                if data_item['name'] == row[3] and categorie == row[2]:
                    data_add = row[0]
                    data_item['data'].append(row[0])
            if not data_add:
                data_item['data'].append(0)

    chart = {
        'chart': {
            'type': 'bar'
        },
        'title': {
            'text': 'Top de asociados'
        },
        'xAxis': {
            'categories': categories
        },
        'yAxis': {
            'title': {
                'text': 'Dolares'
            }
        },
        'series': data

    }

    return JsonResponse(chart)


@login_required
def data3(request):
    oficina = request.GET['point']
    with connection.cursor() as cursor:
        cursor.execute("""  select 
                            ve_descripcion as 'VENDEDOR',
                            (select of_descripcion from tr_oficina o where o.id = v.ve_oficina_id) as 'OFICINA'
                             from tr_vendedor v
                             where ve_oficina_id in (""" + oficina + """)""")
        vendor_rows = cursor.fetchall()
    with connection.cursor() as cursor:
        cursor.execute("""  select 
                            sum(ve_monto)  as 'MONTO',
                            (select ve_descripcion from tr_vendedor v where v.id =a.ve_vendedor_id and v.ve_oficina_id = a.ve_oficina_id) as 'NOMBRE VENDEDOR',
                            (case  substr(ve_fecha,1,2) when '01' then 'ENERO' 
                                                        when '02' then 'FEBRERO'
                                                        when '03' then 'MARZO'
                                                        when '04' then 'ABRIL'
                                                        when '05' then 'MAYO'
                                                        when '06' then 'JUNIO'
                                                        when '07' then 'JULIO'
                                                        when '08' then 'AGOSTO'
                                                        when '09' then 'SEPTIEMBRE'
                                                        when '10' then 'OCTUBRE'
                                                        when '11' then 'NOVIEMBRE'
                                                        when '12' then 'DICIEMBRE' else '' end )    as 'FECHA',
                            (select of_descripcion from tr_oficina where id = a.ve_oficina_id) as 'OFICINA'
                            from tr_ventas a
                            where ve_oficina_id in (""" + oficina + """)
                            group by ve_vendedor_id,substr(ve_fecha,1,2)
                            order by substr(ve_fecha,1,2)""")
        rows = cursor.fetchall()
    data = []
    for vendor in vendor_rows:
        item = {}
        item['name'] = vendor[0]
        item['data'] = []
        data.append(item)
    for row in rows:
        for data_item in data:
            if data_item['name'] == row[1]:
                data_item['data'].append(row[0])
    chart = {
        'title': {
            'text': 'Ventas por mes'
        },

        'xAxis': {
            'categories': ['Ene', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
        },

        'yAxis': {
            'title': {
                'text': 'Total en Dolares'
            }
        },
        'legend': {
            'layout': 'vertical',
            'align': 'right',
            'verticalAlign': 'middle'
        },

        'plotOptions': {
            'series': {
                'label': {
                    'connectorAllowed': False
                }
            }
        },

        'series': data,

        'responsive': {
            'rules': [{
                'condition': {
                    'maxWidth': 500
                },
                'chartOptions': {
                    'legend': {
                        'layout': 'horizontal',
                        'align': 'center',
                        'verticalAlign': 'bottom'
                    }
                }
            }]
        }
    }

    return JsonResponse(chart)


@login_required
def data4(request):
    oficina = request.GET['point']
    with connection.cursor() as cursor:
        cursor.execute("""  select 
                            (select pa_descripcion from tr_pais p ,tr_cliente c where ve_cliente_id = c.id and cl_pais_id =p.id ) as Pais
                            from tr_ventas v 
                            where ve_oficina_id in (""" + oficina + """)
                            group by Pais Order by Pais asc""")
        country_rows = cursor.fetchall()
    with connection.cursor() as cursor:
        cursor.execute("""  select 
                            count(ve_paquete_id)  as TOTALVENDIDOS,
                            (select ti_descripcion from tr_tipo t  ,tr_paquete b, tr_asociado a    where pa_tipo_id    =t.id and b.id= a.ve_paquete_id and pa_asociado_id = a.id ) as TIPOPAQUETE,
                            (select pa_descripcion from tr_pais p ,tr_cliente c where ve_cliente_id = c.id and cl_pais_id =p.id ) as PAIS_ORIGEN
                            from  tr_ventas a
                            where ve_oficina_id in (""" + oficina + """)
                            group by TIPOPAQUETE,PAIS_ORIGEN
                            order by PAIS_ORIGEN,TIPOPAQUETE ASC""")
        rows = cursor.fetchall()
    data = []
    categories = [country[0] for country in country_rows]
    data.append({'name': 'BASICO', 'data': []})
    data.append({'name': 'MEDIUM', 'data': []})
    data.append({'name': 'PREMIUM', 'data': []})
    for categorie in categories:
        for data_item in data:
            data_add = 0
            for row in rows:
                if data_item['name'] == row[1] and categorie == row[2]:
                    data_add = row[0]
                    data_item['data'].append(row[0])
            if not data_add:
                data_item['data'].append(0)

    chart = {
        'chart': {
            'type': 'column'
        },
        'title': {
            'text': 'Origen de los turistas'
        },
        'xAxis': {
            'categories': categories
        },
        'yAxis': {
            'min': 0,
            'title': {
                'text': 'Cantidad'
            },
            'stackLabels': {
                'enabled': True,
                'style': {
                    'fontWeight': 'bold',
                    'color': 'gray'
                }
            }
        },
        'legend': {
            'align': 'right',
            'x': -30,
            'verticalAlign': 'top',
            'y': 25,
            'floating': True,
            'backgroundColor': 'white',
            'borderColor': '#CCC',
            'borderWidth': 1,
            'shadow': False
        },
        'tooltip': {
            'headerFormat': '<b>{point.x}</b><br/>',
            'pointFormat': '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        'plotOptions': {
            'column': {
                'stacking': 'normal',
                'dataLabels': {
                    'enabled': True,
                    'color': 'white'
                }
            }
        },
        'series': data

    }

    return JsonResponse(chart)
