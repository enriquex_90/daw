function initMap() {

    var map = new google.maps.Map(document.getElementById('map_report'), {
        center: {lat: -2.124952, lng: -79.910412},
        zoom: 11,
    });

    var pos1 = {lat: -2.192758, lng: -79.879854};
    var pos2 = {lat: -2.079664, lng: -79.880200};
    var pos3 = {lat: -2.228972, lng: -79.898508};
    var loc = ['Malecón 2000', 'Samborondon', 'Mall del Sur'];
    var puntos = [pos1, pos2, pos3];
    var marker;
    var marker2;
    var marker3;
    // document.getElementById('report_title').innerHTML = loc[0];
    get_data(1);

    //for (var i = 0; i < puntos.length; i++) {
    marker = new google.maps.Marker({
        position: puntos[0],
        map: map,
        title: loc[0]
    });
    marker.addListener('click', function () {
        get_data(1);
        // document.getElementById('report_title').innerHTML = loc[0];
    });

    marker2 = new google.maps.Marker({
        position: puntos[1],
        map: map,
        title: loc[1]
    });
    marker2.addListener('click', function () {
        get_data(2);
        // document.getElementById('report_title').innerHTML = loc[1];
    });


    marker3 = new google.maps.Marker({
        position: puntos[2],
        map: map,
        title: loc[2]
    });
    marker3.addListener('click', function () {
        get_data(3);
        // document.getElementById('report_title').innerHTML = loc[2];
    });

    function get_data(point) {
        $.ajax({
            url: $("#graph1").attr("data-url"),
            dataType: 'json',
            data: {'point': point},
            success: function (data) {
                Highcharts.chart("graph1", data);
            }
        });
        $.ajax({
            url: $("#graph2").attr("data-url"),
            dataType: 'json',
            data: {'point': point},
            success: function (data) {
                Highcharts.chart("graph2", data);
            }
        });
        $.ajax({
            url: $("#graph3").attr("data-url"),
            dataType: 'json',
            data: {'point': point},
            success: function (data) {
                Highcharts.chart("graph3", data);
            }
        });
        $.ajax({
            url: $("#graph4").attr("data-url"),
            dataType: 'json',
            data: {'point': point},
            success: function (data) {
                Highcharts.chart("graph4", data);
            }
        });
    }
}