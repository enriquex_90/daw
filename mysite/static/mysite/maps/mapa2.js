  	 function initMap() {
             	 
		var contentString1 = '<p><b>Mall del Sol</b>, .................. </p>';
		var contentString2 = '<p><b>Malecón 2000</b>, .................. </p>';
		var contentString3 = '<p><b>Mall del Sur</b>, .................. </p>';
		var map = new google.maps.Map(document.getElementById('map'), {
		  center: {lat: -2.196298, lng: -79.910412},

          zoom: 12,
        });

		var pos1 = {lat: -2.192758, lng: -79.879854};
		var pos2 = {lat: -2.155028, lng: -79.892541};
		var pos3 = {lat: -2.228972, lng: -79.898508};
		var loc = ['Malecón 2000', 'Mall del Sol','Mall del Sur'];
		var puntos = [pos1, pos2, pos3];
		var descrip = [contentString1, contentString2,contentString3];
		var marker;
		var marker2;
		var marker3;
		var infowindow;
		var infowindow2;
		var infowindow3;

		marker = new google.maps.Marker({
          position: puntos[0],
          map: map,
		  title: loc[0]
		});
		infowindow = new google.maps.InfoWindow({
          content: descrip[1]
        });
		marker.addListener('click', function() {
          //infowindow.open(map, marker);
          document.getElementById('video').src = "https://www.youtube.com/embed/8KB5_1SkvDM";
        });

		marker2 = new google.maps.Marker({
          position: puntos[1],
          map: map,
		  title: loc[1]
		});
		infowindow2 = new google.maps.InfoWindow({
          content: descrip[0]
        });
		marker2.addListener('click', function() {
          //infowindow2.open(map, marker2);
          document.getElementById('video').src = "https://www.youtube.com/embed/z74uA_l6lsM";
        });

		marker3 = new google.maps.Marker({
          position: puntos[2],
          map: map,
		  title: loc[2]
		});
		infowindow3 = new google.maps.InfoWindow({
          content: descrip[2]
        });
		marker3.addListener('click', function() {
          //infowindow3.open(map, marker3);
          document.getElementById('video').src = "https://www.youtube.com/embed/oIcLh4YbVMg";
        });
      }