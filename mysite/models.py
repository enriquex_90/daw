from django.db import models

class Pais(models.Model):
    class Meta:
        db_table = 'tr_pais'

    pa_descripcion = models.CharField(max_length=30)


class Oficina(models.Model):
    class Meta:
        db_table = 'tr_oficina'

    of_descripcion = models.CharField(max_length=30)
    of_estado = models.CharField(max_length=5)


class Asociado(models.Model):
    class Meta:
        db_table = 'tr_asociado'

    as_descripcion = models.CharField(max_length=30)
    as_estado = models.CharField(max_length=5)


class Cliente(models.Model):
    class Meta:
        db_table = 'tr_cliente'

    cl_passport = models.CharField(max_length=30)
    cl_nombre = models.CharField(max_length=30)
    cl_pais = models.ForeignKey(Pais, on_delete=models.CASCADE)


class Vendedor(models.Model):
    class Meta:
        db_table = 'tr_vendedor'

    ve_descripcion = models.CharField(max_length=30)
    ve_estado = models.CharField(max_length=5)
    ve_oficina = models.ForeignKey(Oficina, on_delete=models.CASCADE)

class Tipo(models.Model):
    class Meta:
        db_table = 'tr_tipo'

    ti_descripcion = models.CharField(max_length=30)


class Paquete(models.Model):
    class Meta:
        db_table = 'tr_paquete'

    pa_descripcion = models.CharField(max_length=30)
    pa_tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    pa_precio_pvp = models.FloatField()
    pa_asociado = models.ForeignKey(Asociado, on_delete=models.CASCADE)


class Ventas(models.Model):
    class Meta:
        db_table = 'tr_ventas'

    ve_vendedor = models.ForeignKey(Vendedor, on_delete=models.CASCADE)
    ve_paquete = models.ForeignKey(Paquete, on_delete=models.CASCADE)
    ve_cliente = models.ForeignKey(Cliente, on_delete=models.CASCADE)
    ve_monto = models.FloatField()
    ve_fecha = models.DateField()
    ve_oficina = models.ForeignKey(Oficina, on_delete=models.CASCADE)